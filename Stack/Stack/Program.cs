﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<string> myStack = new Stack<string>(5);

            myStack.Push("Film 5");
            myStack.Push("Film 4");
            myStack.Push("Film 3");
            myStack.Push("Film 2");
            myStack.Push("Film 1");
            myStack.Push("Film 0");

            WriteLine("===============\n Stack'in en üstündeki eleman : ");
            WriteLine(myStack.Peek());

            WriteLine("\nDizinin en üstündeki elemanı çıkardım ve dizinin yeni hali aşağıdaki gibi : \n");
            myStack.Pop();
            while (!myStack.IsEmpty())
            {
                WriteLine(myStack.Pop());
            }

            WriteLine("===============\n Stack'in tüm elemanları aşağıdadır : ");
            while (!myStack.IsEmpty())
            {
                WriteLine(myStack.Pop());
            }

            //myStack.Pop();
        }
    }

    public class Stack<T>
    {
        private int maxSize; // yığının maksimum alabileceği eleman sayısı
        private T[] myStack; // girilen tipe göre oluşturulacak yığının adı
        private int top; // yığının en üst noktası

        public Stack(int size) // yığının yapılandırıcı metoduna gerekli başlangıç bilgilerini giriyoruz
        {
            maxSize = size; // yığının maksimum elaman sayısını atıyoruz
            myStack = new T[maxSize]; // atanan maksimum sayıya göre dizimizi oluşturuyoruz
            top = -1; // yığının tepe noktasını başlangıç olarak -1 yapıyoruz (eleman atandığında 0 dan başlayabilelim diye)
        }

        public void Push(T data) // yığına eleman iteleme (yığına eleman iteleme tepe noktasından olur)
        {
            if (IsFull()) // yığın dolu mu diye kontrol ediyoruz
            {
                WriteLine("Yığın zaten dolu");
            }
            else // dolu değilse
            {
                top++; // tepe noktamızı 1 arttırıyoruz
                myStack[top] = data; // tepe noktamıza girilen elemanı atıyoruz
            }
        }

        public T Pop() // yığından eleman çıkarma (yığından eleman çıkarma tepe noktasından olur)
        {
            if (IsEmpty()) // yığının boş olup olmadığına bakıyoruz
            {
                WriteLine("Yığın zaten boş");
                return (T)Convert.ChangeType("???", typeof(T));
            }
            else // boş değilse
            {
                int oldTop = top; // tepe noktasını geçici bir değişkene atıyoruz
                top--; // tepe noktasını 1 azaltıyoruz
                return myStack[oldTop]; // geçici değişkendeki elemanı döndürüyoruz
            }
        }

        public T Peek() // yığının tepe noktasındaki elemana bakma
        {
            if (IsEmpty()) // yığının boş olup olmadığına bakıyoruz
            {
                WriteLine("Yığın zaten boş");
                return (T)Convert.ChangeType("???", typeof(T));
            }
            else // boş değilse
            {
                return myStack[top]; // yığının tepe noktasındaki elemanı döndürüyoruz
            }
        }

        public bool IsEmpty() // yığının boş olup olmadığını kontrol etme
        {
            return (top == -1); // tepe noktası -1 ise boştur
        }

        private bool IsFull() // yığının dolu olup olmadığını kontrol etme
        {
            return (top + 1 == maxSize); // tepe noktasını -1 ile başlattığımız için eğer 1 fazlası maksimum eleman sayısına eşitse doludur (yada top == maxSize -1)
        }
    }
}
