﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Program
    {
        static void Main(string[] args)
        {

            Queue<int> myQueueList = new Queue<int>(5);
            myQueueList.Insert(1);
            myQueueList.Insert(2);
            myQueueList.Insert(3);
            myQueueList.Insert(4);
            myQueueList.Insert(5);
            myQueueList.Insert(6);
            myQueueList.View();

            Console.WriteLine("\nKuyruğun eleman sayısı : "+myQueueList.item);
            Console.WriteLine("\nKuyruğun en önündeki eleman : " + myQueueList.PeekFront());

            myQueueList.Remove();
            Console.WriteLine("\nKuyruktan eleman sildim ve şimdi en öndeki elaman : " + myQueueList.PeekFront());
        }
    }

    public class Queue<T>
    {
        private int maxSize; // Kuyruğun eleman sayısı
        private T[] myQueue; // alacağı tipe göre oluşacak kuyruğun adı
        private int front; // kuyruğun önü
        private int rear; // kuyruğun arkası
        public int item; // kuyruktaki toplam eleman sayısı

        public Queue(int size) // kuyruğun yapılandırıcı metodu
        {
            maxSize = size; // girilen değere göre maksimum eleman sayımızı atıyoruz
            myQueue = new T[maxSize]; // maksimum eleman sayımıza göre kuyruğumuzu oluşturuyoruz
            front = 0; // kuyruğun önünü sıfır yapıyoruz
            rear = -1; // kuyruğun arkasını -1 yapıyoruz
            item = 0; // kuyruktaki eleman sayısını 0 yapıyoruz
        }

        public void Insert(T data) // kuyruğa eleman ekleme (kuyruğa ekleme arkadan olur)
        {
            if (IsFull()) // kuyruk dolu mu diye kontrol ediyoruz
            {
                Console.WriteLine("Kuyruk zaten dolu");
            }
            else // kuyruk dolu değilse
            {
                if (rear == maxSize - 1) // arkamızın maksimuma ulaşıp ulaşmadığını kontrol ediyoruz
                {
                    rear = -1; // ulaştıysa kuyruğu tekrar -1 yaparak diziyi tekrar kullanılabilir hale getiriyoruz.
                }
                rear++; // arkayı 1 arttırıyoruz
                item++; // eleman sayısını 1 arttırıyoruz
                myQueue[rear] = data; // girilen elemanı kuyruğun arkasına atıyoruz
            }
        }

        public T Remove() // kuyruktan eleman silme (kuyruktan eleman silme önden olur)
        {
            if (IsEmpty()) // kuyruk boş mu diye kontrol ediyoruz
            {
                Console.WriteLine("Kuyruk zaten boş");
                return (T)Convert.ChangeType("???", typeof(T));
            }
            else // kuyruk boş değilse
            {
                int oldFront = front; // kuyruğun önündeki değeri geçici bir değere atıyoruz
                front++; // önü 1 arttırıyoruz
                item--; // eleman sayısını 1 azaltıyoruz
                if (front == maxSize) // eğer kuyruğun önü, maksimum eleman sayımıza ulaştıysa
                {
                    front = 0; // önü tekrar sıfırlayarak diziyi kullanılabilir hale getiriyoruz
                }
                return myQueue[oldFront]; // kuyruğun önündeki elemanı döndürüyoruz
            }
        }

        public T PeekFront() // kuyruğun en önündeki elemana bakma
        {
            if (IsEmpty()) // kuyruk boş mu diye kontrol ediyoruz
            {
                Console.WriteLine("Kuyruk zaten boş");
                return (T)Convert.ChangeType("???", typeof(T));
            }
            else // kuyruk boş değilse
            {
                return myQueue[front]; // en öndeki elemanı döndürüyoruz
            }
        }

        public void View() // kuyruğun elemanlarına bakmak
        {
            Console.Write("[");
            for (int i = 0; i < myQueue.Length; i++) // dizinin tüm elemanlarını for döngüsüyle gezerek
            {
                Console.Write(myQueue[i] + " "); // konsola aralarında birer boşluk ile yazrıyoruz
            }
            Console.Write("]");
        }

        private bool IsEmpty() // kuyruğun boş olup olmadığını kontrol etme
        {
            return (item == 0); // eğer içindeki eleman sayısı sıfır ise boştur
        }

        private bool IsFull() // kuyruğun dolu olup olmadığını kontrol etme
        {
            return (item == maxSize); // eğer içindeki eleman sayısı maksimum sayıya ulaşmışsa doludur.
        }
    }
}
