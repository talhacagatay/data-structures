﻿using System;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            SinglyLinkedList<string> myList = new SinglyLinkedList<string>();

            myList.InsertFirst("ÜÇ");
            myList.InsertFirst("İKİ");
            myList.InsertFirst("BİR");
            myList.DisplayList();

            myList.DeleteFirst();
            myList.DisplayList();

            myList.InsertLast("DÖRT");
            myList.DisplayList();

            myList.InsertLast("BEŞ");
            myList.DisplayList();
        }
    }

    public class SinglyLinkedList<T> // tekli liste sınıfımız
    {
        private Node<T> first; // ilk düğümümüz

        public bool IsEmpty() // boş mu diye kontrol ediyoruz
        {
            return (first == null); // ilk düğüm boşsa lise boştur
        }

        public void InsertFirst(T data) // Listenin başına ekleme yapmak
        {
            Node<T> newNode = new Node<T>(); // yeni bir düğüm oluşturuyoruz
            newNode.data = data; // yeni düğümün verisine girilen veriyi atıyoruz
            newNode.next = first; // yeni düğümün kendinden sonra tutacağı veriye, önceki düğümü yani first'ü atıyoruz
            first = newNode; // şimdide first değişkenimize yeni oluşan düğümü atarak first değişkenimizi her zaman en baştaki elemanda tutuyoruz
        }

        public Node<T> DeleteFirst() // listenin ilk elemanını silme
        {
            Node<T> temp = first; // geçici bir düğüm oluşturup en öndeki düğümü buna atıyoruz
            first = first.next; // en öndeki düğüme kendinden sonraki düğümü atıyoruz
            return temp; // geçici tutulan düğümü döndürüyoruz
        }

        public void DisplayList() // listedeki düğümlerin verisini ekrana yazdırıyoruz
        {
            Node<T> current = first; // geçici bir değişken oluşturup öndeki düğümü atıyoruz
            while (current != null) // geçici düğüm null olana kadar
            {
                current.DisplayNode(); // geçici düğümün DisplayNode() metoduyla kendisinin içerdiği veriyi ekrana yazdırıyoruz
                current = current.next; // yazdırdıktan sonra geçici düğümü kendinden bir sonraki elemana atıyoruz
            }
            Console.WriteLine(); // listedeki tüm düğümleri yazdırdıktan sonra bir boşluk bırakmak için
        }

        public void InsertLast(T data) // listenin sonuna eleman eklemek (listenin sonuna eleman eklemek için tüm düğümleri dolaşıp son elemanına erişi onun next düğümüne yeni düğümü atamamız gerekiyor)
        {
            Node<T> current = first; // geçici bir düğüm oluşturup öndeki düğümü atıyoruz
            while (current.next != null) // geçici düğümün next i yani geçici düğümden sonraki düğümü null olana kadar
            {
                current = current.next; // geçici düğümü, kendinden sonraki düğüme atıyoruz. while döngüsü bittiğinde current değişkenimiz son düğümümüz olacaktır.
            }
            Node<T> newNode = new Node<T>(); // yeni eklenecek düğümü oluşturuyoruz
            newNode.data = data; // yeni düğümün verisine girilen veriyi atıyoru
            current.next = newNode; // geçici düğümümüzün (en son düğümümüzün) next(sonraki) düğümüne yeni düğümümüzü atıyoruz yani sona eklemiş oluyoruz 
        }

    }

    public class Node<T> // düğüm sınıfımız (düğümler 2 parçadan oluşur birincisi verinin kendisi ikincisi ise kendinden sonraki düğüm(tekli listeler için böyle))
    {
        public T data; // düğümün tutacağı veri
        public Node<T> next; // düğümün tutacağı kendinden sonraki düğüm

        public void DisplayNode() // düğümün kendi verisini yazdırma metodu
        {
            Console.WriteLine("<" + data + ">");
        }
    }
}
